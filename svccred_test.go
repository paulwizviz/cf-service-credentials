package main

import (
	"code.cloudfoundry.org/cli/plugin"
	"code.cloudfoundry.org/cli/plugin/models"
	"testing"
)

// CliConnection mock
type MockCliConnection struct {
	methodsCalled []string
	isLoggedIn    func() (bool, error)
}

func (m *MockCliConnection) CliCommandWithoutTerminalOutput(args ...string) ([]string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) CliCommand(args ...string) ([]string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetCurrentOrg() (plugin_models.Organization, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetCurrentSpace() (plugin_models.Space, error) {
	panic("not implemented")
}
func (m *MockCliConnection) Username() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) UserGuid() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) UserEmail() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) IsLoggedIn() (bool, error) {
	return m.isLoggedIn()
}
func (m *MockCliConnection) IsSSLDisabled() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasOrganization() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasSpace() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) ApiEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) ApiVersion() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasAPIEndpoint() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) LoggregatorEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) DopplerEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) AccessToken() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetApp(name string) (plugin_models.GetAppModel, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetApps() ([]plugin_models.GetAppsModel, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrgs() ([]plugin_models.GetOrgs_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpaces() ([]plugin_models.GetSpaces_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrgUsers(string, ...string) ([]plugin_models.GetOrgUsers_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpaceUsers(string, string) ([]plugin_models.GetSpaceUsers_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetServices() ([]plugin_models.GetServices_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetService(string) (plugin_models.GetService_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrg(string) (plugin_models.GetOrg_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpace(string) (plugin_models.GetSpace_Model, error) {
	panic("not implemented")
}

// Mock SpawnVcapServices
var mockSpawnVcapServiceShellCalled string

func mockSpawnVcapServiceShell(appName string, cliConnection plugin.CliConnection) error {
	mockSpawnVcapServiceShellCalled = "called"
	return nil
}

var mockSpawnServiceCredentialShellCalled string

func mockSpawnServiceCredentialShell(serviceName string, serviceKey string, cliConnection plugin.CliConnection) error {
	mockSpawnServiceCredentialShellCalled = "called"
	return nil
}

// Test case
func TestRunVSShell(t *testing.T) {
	svcCred := new(cfSvcCred)
	svcCred.spawnVcapServiceShell = mockSpawnVcapServiceShell
	mockCliConnection := new(MockCliConnection)

	// Set the cliConnection to say it has been login
	mockCliConnection.isLoggedIn = func() (bool, error) {
		return true, nil
	}

	// After login and command name is correct, the mock shell will be spawned
	mockSpawnVcapServiceShellCalled = "not called"
	svcCred.Run(mockCliConnection, []string{"vsshell", "appname"})
	if mockSpawnVcapServiceShellCalled == "not called" {
		t.Fatalf("Expected: called Got: %s", mockSpawnVcapServiceShellCalled)
	}
}

func TestRunSVKShell(t *testing.T) {
	svcCred := new(cfSvcCred)
	svcCred.spawnServiceCredentialShell = mockSpawnServiceCredentialShell
	mockCliConnection := new(MockCliConnection)

	// Set the cliConnection to say it has been login
	mockCliConnection.isLoggedIn = func() (bool, error) {
		return true, nil
	}

	// After login and command name is correct, the mock shell will be spawned
	mockSpawnServiceCredentialShellCalled = "not called"
	svcCred.Run(mockCliConnection, []string{"svkshell", "serviceName", "serviceKey"})
	if mockSpawnServiceCredentialShellCalled == "not called" {
		t.Fatalf("Expected: called Got: %s", mockSpawnVcapServiceShellCalled)
	}
}
