# Introduction

A Cloud Foundry (CF) cli plugin to help you extract credentials from CF backing services such as cloudant, blockchain, etc., and spawn a shell with service credential embedded as environmental variable.

# Use case

With this plugin, you could extract VCAP_SERVICES in two ways:

* `bx cf vsshell <appname>` or `cf vsshell <appname>`
* `bx cf svkshell <service-label> <service-key>` or `cf svkshell <service-label> <service-key>` 

#### `bx cf vsshell <appname>` or `cf vsshell <appname>`

This particular command has the same effect as `cf env <appname>`.

Assuming that you have an app, named `very-simple-app` bound to cloudant, when you run this command:

```
parent shell > bx cf vsshell very-simple-app
Invoking 'cf vsshell very-simple-app'...

<user name>@<machine name>:<current path> # 

<user name>@<machine name>:<current path> # echo $VCAP_SERVICES
{"cloudantNoSQLDB":[{"credentials":{"host":"<url>","password":"<password>","port":<port>,"url":"<url>","username":"<username>"},"label":"cloudantNoSQLDB","name":"<service-name>","plan":"Lite","provider":null,"syslog_drain_url":null,"tags":["data_management","ibm_created","lite","ibm_dedicated_public"],"volume_mounts":[]}]}
<user name>@<machine name>:<current path> #
``` 

**Note:** The above example only show credential of one service. However, if your app is bound to more than one services, `$VCAP_SERVICES` will return credentials of all your bounded services. 

#### `bx cf svkshell <service-label> <service-key>` or `cf svkshell <service-label> <service-key>`

If you plan to extract only one particular service and wrapp in $VCAP_SERVICE, when run this command:

```
parent shell > bx cf svkshell test-cloudant cred
Invoking 'cf svkshell test-cloudant cred'...

<user name>@<machine name>:<current path> # 

<user name>@<machine name>:<current path> # echo $VCAP_SERVICES
{"cloudantNoSQLDB":[{"credentials": { "host": "<url>", "password": "<password>", "port": <port>, "url": "<url>", "username": "<username>"}}]}
<user name>@<machine name>:<current path> #
```

**Note:** In this case, $VCAP_SERVICES will only return credential of one particular service as specified in your command line argument.

# Installing plugin

Follow these steps:

1) Assuming that you have CF cli installed and GOPATH set, navigate to $GOPATH and you have access to this repo or your forked version.

1) Run `go get git.ng.bluemix.net/ibm-blockchain/cf-service-credentials` or `go get <your-forked-version>`.

1) A built version of the plugin should be found in `$GOPATH/bin/cf-service-credentials`, if not navigate to plugin git repo and run `go install`.

1) Run CF cli `bx cf install-plugin $GOPATH/bin/cf-service-credentials` or `cf install-plugin $GOPATH/bin/cf-service-credentials` to install the plugin.

# Disclaimer

Unless otherwise specified, this plugin is provided on "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied and its source code is distributed under Apache 2 license.