package servicekey

import (
	"code.cloudfoundry.org/cli/plugin/models"
	"strings"
	"testing"
)

// CliConnection mock
type MockCliConnection struct {
	cliCommandWithoutTerminalOutputResponse func() ([]string, error)
}

func (m *MockCliConnection) CliCommandWithoutTerminalOutput(args ...string) ([]string, error) {

	return m.cliCommandWithoutTerminalOutputResponse()
}
func (m *MockCliConnection) CliCommand(args ...string) ([]string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetCurrentOrg() (plugin_models.Organization, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetCurrentSpace() (plugin_models.Space, error) {
	panic("not implemented")
}
func (m *MockCliConnection) Username() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) UserGuid() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) UserEmail() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) IsLoggedIn() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) IsSSLDisabled() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasOrganization() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasSpace() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) ApiEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) ApiVersion() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasAPIEndpoint() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) LoggregatorEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) DopplerEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) AccessToken() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetApp(name string) (plugin_models.GetAppModel, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetApps() ([]plugin_models.GetAppsModel, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrgs() ([]plugin_models.GetOrgs_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpaces() ([]plugin_models.GetSpaces_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrgUsers(string, ...string) ([]plugin_models.GetOrgUsers_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpaceUsers(string, string) ([]plugin_models.GetSpaceUsers_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetServices() ([]plugin_models.GetServices_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetService(string) (plugin_models.GetService_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrg(string) (plugin_models.GetOrg_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpace(string) (plugin_models.GetSpace_Model, error) {
	panic("not implemented")
}

// Test cases

// Mock service key response
var serviceKeyResponse = []string{
	"Getting key cred for service instance service-name as name@email...",
	"",
	"{",
	`"something": "something"`,
	"}",
}

func TestExtractCredential(t *testing.T) {

	mockCliConnection := new(MockCliConnection)
	mockCliConnection.cliCommandWithoutTerminalOutputResponse = func() ([]string, error) {
		return serviceKeyResponse, nil
	}

	actual, err := extractServiceCredentialFromKey("service name", "key", mockCliConnection)
	if err != nil {
		t.Fatalf("Expect: no error Got: %v", err)
	}

	expected := strings.Join(serviceKeyResponse[2:], "")

	if actual != expected {
		t.Fatalf("Expected: %s Got: %s", expected, actual)
	}

}

func TestCeateVcapFormat(t *testing.T) {
	input := strings.Join(serviceKeyResponse[2:], "")

	expected := `{"serviceType":[{"credentials":{"something": "something"}}]}`

	actual := createVcapFormat("serviceType", "test-service", input)

	if expected != actual {
		t.Fatalf("Expected: %s Got: %s", expected, actual)
	}

}

var serviceTypeResponse = []string{
	"",
	"Service instance: test-cloudant",
	"Service: cloudantNoSQLDB",
	"Bound apps: very-simple-app",
}

func TestExtractServiceType(t *testing.T) {

	mockCliConnection := new(MockCliConnection)
	mockCliConnection.cliCommandWithoutTerminalOutputResponse = func() ([]string, error) {
		return serviceTypeResponse, nil
	}

	actual, err := extractServiceType("hello", mockCliConnection)
	if err != nil {
		t.Fatalf("Expected: no error Got: %s", err)
	}

	if actual != "cloudantNoSQLDB" {
		t.Fatalf("Expected: cloudantNoSQLDB Got: %s", actual)
	}
}
