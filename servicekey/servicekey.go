package servicekey

import (
	"code.cloudfoundry.org/cli/plugin"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func SpawnServiceCredentiallShell(serviceName string, serviceKey string, cliConnection plugin.CliConnection) error {

	credentials, err := extractServiceCredentialFromKey(serviceName, serviceKey, cliConnection)
	if err != nil {
		return err
	}

	serviceType, err := extractServiceType(serviceName, cliConnection)
	if err != nil {
		return err
	}

	vcapServiceEnv := createVcapFormat(serviceType, serviceName, credentials)

	return createShell(vcapServiceEnv)

}

func extractServiceType(serviceName string, cliConnection plugin.CliConnection) (string, error) {

	output, err := cliConnection.CliCommandWithoutTerminalOutput("service", serviceName)
	if err != nil {
		return "", err
	}

	serviceType := output[2][9:]
	return serviceType, nil
}

func extractServiceCredentialFromKey(serviceName string, serviceKey string, cliConnection plugin.CliConnection) (string, error) {

	output, err := cliConnection.CliCommandWithoutTerminalOutput("service-key", serviceName, serviceKey)
	if err != nil {
		return "", err
	}

	credentials := strings.Join(output[2:], "")

	return credentials, nil

}

func createVcapFormat(serviceType string, serviceName string, credentialsJSON string) string {

	vcapJSON := []string{"{", fmt.Sprintf(`"%s":`, serviceType), "[{", `"credentials":`, credentialsJSON, "}]", "}"}
	return strings.Join(vcapJSON, "")

}

func createShell(vcapServices string) error {
	cmd := exec.Command(os.Getenv("SHELL"))
	vcapEnv := fmt.Sprintf("VCAP_SERVICES=%s", vcapServices)
	cmd.Env = append(os.Environ(), "PS1=\\[$(tput setaf 1)\\]\\u@\\h:\\w #\\[$(tput sgr0)\\] ", vcapEnv)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
