package main

import (
	"code.cloudfoundry.org/cli/plugin"
	"fmt"
	"git.ng.bluemix.net/ibm-blockchain/cf-service-credentials/servicekey"
	"git.ng.bluemix.net/ibm-blockchain/cf-service-credentials/vcapsvc"
	"os"
)

const (
	// VSShell is a label for a plugin name to spawn a shell with VCAP_SERVICES embedded
	VSShell = "vsshell"
	// SVKShell is a lable for a plugin name to spawn a shell with service credential
	SVKShell = "svkshell"
)

type cfSvcCred struct {
	spawnVcapServiceShell       func(appName string, cliConnection plugin.CliConnection) error
	spawnServiceCredentialShell func(serviceName string, serviceKey string, cliConnection plugin.CliConnection) error
}

func (c *cfSvcCred) GetMetadata() plugin.PluginMetadata {
	return plugin.PluginMetadata{
		Name: "cf-service-credentials",
		Version: plugin.VersionType{
			Major: 0,
			Minor: 1,
			Build: 0,
		},
		MinCliVersion: plugin.VersionType{
			Major: 6,
			Minor: 7,
			Build: 0,
		},
		Commands: []plugin.Command{
			{
				Name:     VSShell,
				HelpText: "Spawm a shell with VCAP_SERVICES env set",
				UsageDetails: plugin.Usage{
					Usage: fmt.Sprintf("%s\n  cf %s <appname>", VSShell, VSShell),
				},
			},
			{
				Name:     SVKShell,
				HelpText: "Spawm a shell with VCAP_SERVICES env set",
				UsageDetails: plugin.Usage{
					Usage: fmt.Sprintf("%s\n  cf %s <appname>", SVKShell, SVKShell),
				},
			},
		},
	}
}

func (c *cfSvcCred) Run(cliConnection plugin.CliConnection, args []string) {

	if result, err := cliConnection.IsLoggedIn(); err != nil {
		if err != nil {
			fmt.Println(err)
		} else if result == false {
			fmt.Println(`Please login via bx cf login`)
		}
		os.Exit(1)
	}

	if args[0] == VSShell {
		if len(args) == 2 {
			if err := c.spawnVcapServiceShell(args[1], cliConnection); err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			fmt.Println(c.GetMetadata().Commands[0].UsageDetails.Usage)
			os.Exit(1)
		}
	} else if args[0] == SVKShell {
		if len(args) == 3 {
			if err := c.spawnServiceCredentialShell(args[1], args[2], cliConnection); err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			fmt.Println(c.GetMetadata().Commands[1].UsageDetails.Usage)
			os.Exit(1)
		}
	}

}

func main() {
	serviceCred := new(cfSvcCred)
	serviceCred.spawnVcapServiceShell = vcapsvc.SpawnShellWithVCAPService
	serviceCred.spawnServiceCredentialShell = servicekey.SpawnServiceCredentiallShell
	plugin.Start(serviceCred)
}
