package vcapsvc

import (
	"code.cloudfoundry.org/cli/plugin/models"
	"testing"
)

// CliConnection mock
type MockCliConnection struct {
	methodsCalled []string
}

func (m *MockCliConnection) CliCommandWithoutTerminalOutput(args ...string) ([]string, error) {
	m.methodsCalled = append(m.methodsCalled, "CliCommandWithoutTerminalOutput")
	return []string{""}, nil
}
func (m *MockCliConnection) CliCommand(args ...string) ([]string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetCurrentOrg() (plugin_models.Organization, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetCurrentSpace() (plugin_models.Space, error) {
	panic("not implemented")
}
func (m *MockCliConnection) Username() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) UserGuid() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) UserEmail() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) IsLoggedIn() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) IsSSLDisabled() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasOrganization() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasSpace() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) ApiEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) ApiVersion() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) HasAPIEndpoint() (bool, error) {
	panic("not implemented")
}
func (m *MockCliConnection) LoggregatorEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) DopplerEndpoint() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) AccessToken() (string, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetApp(name string) (plugin_models.GetAppModel, error) {

	m.methodsCalled = append(m.methodsCalled, "GetApp")
	return plugin_models.GetAppModel{}, nil

}
func (m *MockCliConnection) GetApps() ([]plugin_models.GetAppsModel, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrgs() ([]plugin_models.GetOrgs_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpaces() ([]plugin_models.GetSpaces_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrgUsers(string, ...string) ([]plugin_models.GetOrgUsers_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpaceUsers(string, string) ([]plugin_models.GetSpaceUsers_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetServices() ([]plugin_models.GetServices_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetService(string) (plugin_models.GetService_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetOrg(string) (plugin_models.GetOrg_Model, error) {
	panic("not implemented")
}
func (m *MockCliConnection) GetSpace(string) (plugin_models.GetSpace_Model, error) {
	panic("not implemented")
}

// Test cases
func TestObtainStagingEnv(t *testing.T) {

	mockCliConnection := new(MockCliConnection)

	mockCliConnection.methodsCalled = make([]string, 0)

	obtainStagingEnv("hello", mockCliConnection)

	actual := len(mockCliConnection.methodsCalled)
	if actual != 2 {
		t.Fatalf("Expect: 2 calls Got: %d", actual)
	}

	firstCall := mockCliConnection.methodsCalled[0]
	if firstCall != "GetApp" {
		t.Fatalf("Expected: GetApp Got: %s", firstCall)
	}

	secondCall := mockCliConnection.methodsCalled[1]
	if secondCall != "CliCommandWithoutTerminalOutput" {
		t.Fatalf("Expected: CliCommandWithoutTerminalOutput Got: %s", secondCall)
	}

}

// Mock staging env
// Note: this is a representative structure
var stagingJSON = []string{"{",
	`"staging_env_json":{"BLUEMIX_REGION":"ibm:yp:eu-gb"},`,
	`"running_env_json":{"BLUEMIX_REGION":"ibm:yp:eu-gb"},`,
	`"environment_json":{},`,
	`"system_env_json":{"VCAP_SERVICES":{"someservice":[{"something":"something"}]}},`,
	`"application_env_json":{}`,
	"}"}

func TestExtractVcapService(t *testing.T) {

	var expected = `{"someservice":[{"something":"something"}]}`

	actual, err := extractVcapService(stagingJSON)
	if err != nil {
		t.Fatalf("Expected: no error Got: %v", err)
	}

	if actual != expected {
		t.Fatalf("Expected: %s Actual: %s", expected, actual)
	}

}
