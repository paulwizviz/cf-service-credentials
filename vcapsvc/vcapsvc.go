package vcapsvc

import (
	"code.cloudfoundry.org/cli/plugin"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func SpawnShellWithVCAPService(appName string, cliConnection plugin.CliConnection) error {

	stagingEnv, err := obtainStagingEnv(appName, cliConnection)
	if err != nil {
		return err
	}

	vcapServiceEnv, err := extractVcapService(stagingEnv)
	if err != nil {
		return err
	}

	return createShell(vcapServiceEnv)
}

func obtainStagingEnv(appName string, cliConnection plugin.CliConnection) ([]string, error) {
	app, err := cliConnection.GetApp(appName)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/v2/apps/%s/env", app.Guid)
	output, err := cliConnection.CliCommandWithoutTerminalOutput("curl", url)
	if err != nil {
		return nil, err
	}

	return output, nil

}

func extractVcapService(input []string) (string, error) {

	val := strings.Join(input, "")

	var vcapService interface{}

	if err := json.Unmarshal([]byte(val), &vcapService); err != nil {
		return "", err
	}

	envJSON := vcapService.(map[string]interface{})
	systemEnvJSON := envJSON["system_env_json"].(map[string]interface{})
	vcapServicesJSON := systemEnvJSON["VCAP_SERVICES"].(map[string]interface{})
	b, err := json.Marshal(vcapServicesJSON)
	return string(b), err
}

func createShell(vcapServices string) error {
	cmd := exec.Command(os.Getenv("SHELL"))
	vcapEnv := fmt.Sprintf("VCAP_SERVICES=%s", vcapServices)
	cmd.Env = append(os.Environ(), "PS1=\\[$(tput setaf 1)\\]\\u@\\h:\\w #\\[$(tput sgr0)\\] ", vcapEnv)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
